//base canvas(variabili+grandezza+font)
const canvas = document.getElementById('canvasA');
const cont = canvas.getContext('2d');
let punteggio = 0;
let framePSG = 0; //meglio mettere null?
let diff = 1;
let gameOver = false;


canvas.width = 900; //si può ingrandire
canvas.height = 600; //si può ingrandire

cont.font = '40px Comic Sans MS, Comic Sans MS5, cursive' 

//segui mouse


var posizione = canvas.getBoundingClientRect(); //restituisce la posizione(left, top, right, bottom, x, y, width, height)
var puntatore = {
    x: canvas.width / 2, y: canvas.height / 2, cliccato: false
}

canvas.addEventListener('mousedown', function (coordinate) { //funzione per trovare le coordinate del click e settare su true "cliccato"
    puntatore.cliccato = true;
    posizione = canvas.getBoundingClientRect(); //restituisce la posizione(left, top, right, bottom, x, y, width, height)
    puntatore = {
        x: canvas.width / 2, y: canvas.height / 2, cliccato: false
    }
    puntatore.cliccato = true;
    puntatore.x = Math.floor(coordinate.x - posizione.left);
    puntatore.y = Math.floor(coordinate.y - posizione.top);
    console.log(puntatore.x, puntatore.y);
});
canvas.addEventListener('mouseup', function () {
    puntatore.cliccato = false;
})
//giocatore+skin(frame = larghezza/colonne + altezza/righe)
const giocatoreSN = new Image();
giocatoreSN.src = 'personaggi/personaggio_sinistra/personaggio_sinistra.png';
const giocatoreDS = new Image();
giocatoreDS.src = 'personaggi/personaggio_destra/personaggio_destra.png';

//Personaggio
class Personaggio {
    constructor() {
        this.x = canvas.width / 2;
        this.y = canvas.height / 2;
        this.raggio = 40;
        this.inclinazione = 0;
        //faccia verso puntatore
        this.direzioneX = 0;
        this.direzioneY = 0;
        this.spriteWidth = 512           //(pixel / colonne)
        this.spriteHeight = 512         //(pixel / righe)
    }
    movimento() {
        const dx = this.x - puntatore.x;
        const dy = this.y - puntatore.y;
        let t = Math.atan2(dy, dx);
        this.inclinazione = t;

        if (puntatore.x != this.x) {
            this.x -= dx / 20;
        }
        if (puntatore.y != this.y) {
            this.y -= dy / 20;
        }

        //Livelli
        if (diff == 4) {
            bg.src = 'immagini/bg/colorati/squares_repeating_blue.png';
            cont.fillStyle = 'white'
            cont.fillText('ora ti TELETRASPORTI', 290, 400);
            Nemico1.vel = Math.random() * 2 + 18;
            if (puntatore.x != this.x) {
                this.x -= dx / 5;
            }
            if (puntatore.y != this.y) {
                this.y -= dy / 5;
            }
        }
        if (diff == 7) {
            bg.src = 'immagini/bg/colorati/waves_glow_purple.png';
            cont.fillStyle = 'white'
            cont.fillText('ora sei LENTISSIMO', 290, 400);
            if (puntatore.x != this.x) {
                this.x -= dx / 600;
            }
            if (puntatore.y != this.y) {
                this.y -= dy / 600;
            }
        }
        if (diff == 12) {
            bg.src = 'immagini/bg/colorati/squares_repeating_green.png';
            cont.fillStyle = 'white'
            cont.fillText('ora sei VELOCISSIMO', 290, 400);
            if (puntatore.x != this.x) {
                this.x -= dx / 10;
            }
            if (puntatore.y != this.y) {
                this.y -= dy / 10;
            }

            
        }
        if(diff == 5) {
            bg.src = 'immagini/bg/colorati/spiral_glow_purple.png'
            cont.fillStyle = 'white'
            cont.fillText('Mostro più VELOCE', 290, 400);
            Nemico1.vel = Math.random() * 2 + 19;
            if (puntatore.x != this.x) {
                this.x -= dx / 20;
            }
            if (puntatore.y != this.y) {
                this.y -= dy / 20;
            }
        }
        if (diff == 15) {
            bg.src = 'immagini/bg/colorati/spiral_repeating_green.png';
            cont.fillStyle = 'white'
            cont.fillText('che succede?', 290, 400);
            Nemico1.vel = Math.random() * 2 + 18;
            if (puntatore.x != this.x) {
                this.x -= dx / 800;
            }
            if (puntatore.y != this.y) {
                this.y -= dy / 2;
            }
        }
        if (diff == 30) {
            bg.src = 'immagini/bg/colorati/waves_repeating_blue.png';
            cont.fillStyle = 'white'
            cont.fillText('che casino!', 290, 400);
            Nemico1.vel = Math.random() * 2 + 15;
            if (puntatore.x != this.x) {
                this.x -= dx / 2;
            }
            if (puntatore.y != this.y) {
                this.y -= dy / 800;
            }
        }
        
    }
    draw() {
        if (puntatore.cliccato) {
            cont.lineWidth = 0.2;
            cont.beginPath();
            cont.moveTo(this.x, this.y);
            cont.lineTo(puntatore.x, puntatore.y);
            cont.stroke();
            
        }



        //rotazione con canvas(copiata perchè troppo complicata)


        cont.save();
        cont.translate(this.x, this.y);
        cont.rotate(this.inclinazione);
        if (this.x >= puntatore.x) {
            cont.drawImage(giocatoreSN, this.direzioneX * this.spriteWidth, this.direzioneY * this.spriteHeight,
                this.spriteWidth, this.spriteHeight, 0 - 55, 0 - 35, this.spriteWidth / 5, this.spriteHeight / 5)
        } else {
            cont.drawImage(giocatoreDS, this.direzioneX * this.spriteWidth, this.direzioneY * this.spriteHeight,
                this.spriteWidth, this.spriteHeight, 0 - 55, 0 - 35, this.spriteWidth / 5, this.spriteHeight / 5)
        }
        cont.restore();
    }

}
const giocatore = new Personaggio();



//oggetto da collezionare

const imgBG = new Image();
var myPix2 = [];

function choosePic2() {
    var caso2 = Math.floor(Math.random() * myPix2.length);
    imgPill.src = myPix2[caso2];
}


const pilloleRY = [];
const imgPill = new Image();
var myPix = ["immagini/pngs/black_pill.png", "immagini/pngs/blue_pill.png", "immagini/pngs/blue_red_pill.png", "immagini/pngs/blue_red_spots_pill.png",
    "immagini/pngs/bright_green_white_pill.png",
    "immagini/pngs/dark_green_white_pill.png", "immagini/pngs/green_blue_spots_pill.png", "immagini/pngs/green_green_spots_pill.png", "immagini/pngs/green_pill.png", "immagini/pngs/green_purple_pill.png",
    "immagini/pngs/green_red_pill.png", "immagini/pngs/green_white_pill.png", "immagini/pngs/light_blue_white_pill.png", "immagini/pngs/orange_pill.png", "immagini/pngs/orange_red_pill.png",
    "immagini/pngs/orange_white_pill.png", "immagini/pngs/pink_green_pill.png", "immagini/pngs/pink_pill.png",
    "immagini/pngs/pink_white.png", "immagini/pngs/pink_yellow_spots_pill.png", "immagini/pngs/red_pill.png", "immagini/pngs/red_yellow_pill.png",
    "immagini/pngs/white_black_spots_pill.png", "immagini/pngs/white_pill.png", "immagini/pngs/yellow_brown_spots_pill.png", "immagini/pngs/yellow_pill.png",
    "immagini/pngs/yellow_red_pill.png", "immagini/pngs/yellow_white_pill.png"];
function choosePic() {
    var caso = Math.floor(Math.random() * myPix.length);
    imgPill.src = myPix[caso];
}
choosePic();
class pillole {
    constructor() {
        this.x = Math.random() * canvas.width;
        this.y = canvas.height + 100 + Math.random() * canvas.height;
        this.raggio = 20;
        this.vel = Math.random() * 6 + 2;
        this.distanza;
        this.hittato = false;
        this.suono = Math.random() <= 0.5 ? 'suono1' : 'suono2';
    }
    muoviPillole() {
        this.y -= this.vel;
        const dx = this.x - giocatore.x;
        const dy = this.y - giocatore.y;
        this.distanza = Math.sqrt(dx * dx + dy * dy);
    }
    draw() {
        cont.drawImage(imgPill, this.x - 15, this.y - 15, this.raggio * 1.5, this.raggio * 1.5);
    }
}

const hittato1 = document.createElement('audio');
hittato1.src = 'suoni/p_5.ogg';
const hittato2 = document.createElement('audio');
hittato2.src = 'suoni/p_2.ogg';

function mngPillole() {
    if (framePSG % 40 == 0) {                         //ogni 50 frame crea un nuovo elemento nell'array
        pilloleRY.push(new pillole());
    }
    for (let i = 0; i < pilloleRY.length; i++) {
        pilloleRY[i].muoviPillole();
        pilloleRY[i].draw();
        if (pilloleRY[i].y < 0 - pilloleRY[i].raggio * 2) {
            pilloleRY.splice(i, 1);
            i--;
        } else if (pilloleRY[i].distanza < pilloleRY[i].raggio + giocatore.raggio) {
            if (!pilloleRY[i].hittato) {
                if (pilloleRY[i].suono == 'suono1') {
                    choosePic();
                    hittato1.play();
                } else {
                    choosePic();
                    hittato2.play();
                }
                punteggio++;
                pilloleRY[i].hittato = true;
                if (punteggio >= 10) {
                    diff = 4;
                }
                if (punteggio >= 20) {
                    diff = 7;
                }
                if (punteggio >= 25) {
                    diff = 12;
                }
                if (punteggio >= 35) {
                    diff = 5;
                }
                if (punteggio >= 45) {
                    diff = 15;
                }
                if (punteggio >= 50){
                    diff == 30;
                }
                pilloleRY.splice(i, 1);
                i--;
                var diffRY = [4, 7, 12, 5, 15, 30];
                if(punteggio > 50){
                    diff = diffRY[Math.floor(Math.random() * 6)];
                    console.log(diff);
                }
            }
        }
    }
}

const bg = new Image();
bg.src = 'immagini/bg/colorati/bobble_repeating_pink.png';

const BACKGROUND = {
    x1: 0,
    x2: canvas.width,
    y: 0,
    maxLar: canvas.width,
    maxAlt: canvas.height
}

function visualizzaBg() {
    BACKGROUND.x1 -= diff;
    if (BACKGROUND.x1 < -BACKGROUND.maxLar)
        BACKGROUND.x1 = BACKGROUND.maxLar;

    BACKGROUND.x2 -= diff;
    if (BACKGROUND.x2 < -BACKGROUND.maxLar)
        BACKGROUND.x2 = BACKGROUND.maxLar;

    cont.drawImage(bg, BACKGROUND.x1, BACKGROUND.y, BACKGROUND.maxLar, BACKGROUND.maxAlt);
    cont.drawImage(bg, BACKGROUND.x2, BACKGROUND.y, BACKGROUND.maxLar, BACKGROUND.maxAlt);

}
//Nemico
const imgNemico = new Image();
imgNemico.src = 'immagini/animazione_mostro.png'

class Nemico {
    constructor() {
        this.x = canvas.width + 200;
        this.y = Math.random() * (canvas.height - 150) + 90;
        this.raggio = 60;
        this.vel = Math.random() * 2 + 2;
        this.frame = 0;
        this.direzioneX = 0;
        this.direzioneY = 0;
        this.spriteWidth = 575
        this.spriteHeight = 542
    }
    disegna() {
        cont.drawImage(imgNemico, this.direzioneX * this.spriteWidth, this.direzioneY * this.spriteHeight, this.spriteWidth, this.spriteHeight, this.x - 60, this.y - 55,
            this.raggio * 2, this.raggio * 2)
    }
    agg() {
        this.x -= this.vel;
        if (this.x < 0 - this.raggio * 2) {
            this.x = canvas.width + 200;
            this.y = Math.random() * (canvas.height - 150) + 90;
            this.vel = Math.random() * 2 + 2;
        }
        if (framePSG % 4 == 0) {
            this.frame++;
            if (this.frame >= 16)
                this.frame = 0;
            if (this.frame == 3 || this.frame == 7 || this.frame == 11 || this.frame == 15) {
                this.direzioneX = 0;
            } else {
                this.direzioneX++;
            }
            if (this.frame < 3) this.direzioneY = 0;
            else if (this.frame < 7) this.direzioneY = 1;
            else if (this.frame < 11) this.direzioneY = 2;
            else if (this.frame < 15) this.direzioneY = 3;
            else this.direzioneY = 0;
        }

        const dx = this.x - giocatore.x;
        const dy = this.y - giocatore.y;
        const distanza1 = Math.sqrt(dx * dx + dy * dy);
        if (distanza1 < this.raggio + giocatore.raggio) {
            fine();
        }
    }

}
const Nemico1 = new Nemico();
function muoviNem() {
    Nemico1.disegna();
    Nemico1.agg();
}

function fine() {
    cont.fillStyle = 'white';
    cont.fillText('HAI PERSO, il tuo punteggio è di ' + punteggio, 130, 250);
    gameOver = true;
}

//animazioni
function anima() {
    cont.clearRect(0, 0, canvas.width, canvas.height);
    visualizzaBg();
    mngPillole();
    giocatore.movimento();
    giocatore.draw();
    muoviNem();
    cont.fillStyle = 'black'
    cont.fillText('punteggio: ' + punteggio, 10, 50);
    framePSG++;
    if (!gameOver) {
        requestAnimationFrame(anima)
    }
}
anima();


