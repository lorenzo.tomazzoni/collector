# COLLECTOR
## PER MAGGIORI INFORMAZIONI CONTATTARE
# [Tomazzoni Lorenzo](https://mail.google.com/mail/u/0/#inbox?compose=GTvVlcSHxwLWktvRGhHLRqKPGMPnxhtWcKxrljSZLvtgptzDrgFTlFCPRgZDfNCwrvgKBGmvZDDPB) [Tommaso Civettini](https://mail.google.com/mail/u/2/#inbox?compose=GTvVlcSHxThnPZfbhPXGHLJGXjvWzQPPVSgjdqcMnftckLpWHLBjBGgHlxcZMDGNJGtWbWWcRMGSL)



Gioco molto semplice creato con l'utilizzo di phaser e la mente geniale di Tomazzoni e Civettini.

## Features

- Molti livelli con difficoltà aggiuntive
- Comandi molto semplici

> In questo gioco si è un mostro blu molto simpatico (Franco) che deve muoversi 
> in uno spazio di gioco raccogliendo delle pillole che si muovono in 
> maniera casuale nello schermo, evitando un mostro viola un po' meno simpatico (Gianni). Ogni volta
> che si colleziona una di queste pillole si guadagna 1 punto. Una volta 
> raggiunto un certo numero di punti si passa ad un livello successivo, 
>ognuno con una peculiare difficoltà.

## Installazione
- step 1
Collector richiede [Visual studio code](https://code.visualstudio.com) per essere avviato.

- step 2
```sh
cd percorso installazione
git clone https://gitlab.com/lorenzo.tomazzoni/collector.git
```
## Plugin

_PROSSIMAMENTE_